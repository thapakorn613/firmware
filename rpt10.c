//
// gogo40.c - Contains the core functions of the gogo board firmware
//
// Copyright (C) 2001-2008 Massachusetts Institute of Technology
//           (C) 2008 onwards Chiang Mai University, Thailand
// Contact   Arnan (Roger) Sipiatkiat [arnans@gmail.com]
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//



#case



//#include <18F66J94.H>
//#define PIC18F66J94

#include <18F66J50.H>
#define PIC18F66J50

#device ADC=10 *=16

#ifdef PIC18F66J50
   #FUSES NOWDT, WDT128, PLL5, HSPLL, NOCPUDIV, NOXINST, NOIESO, NOPROTECT, CCP2E7
   #use delay(clock=48M)
#endif

#ifdef PIC18F66J94
   #FUSES NOWDT,PLL5, PR_PLL, HS, NOXINST, NOIESO, NOPROTECT, NOVBATBOR
   #use delay(clock=64M)
   #PIN_SELECT U1RX=PIN_C7
   #PIN_SELECT U1TX=PIN_C6
   #PIN_SELECT P1A=PIN_C2   // map pin CCP1 - beeper
   #PIN_SELECT CCP2=PIN_E7  // IR input

//!   #PIN_SELECT P3A=PIN_D1
//!   #PIN_SELECT CCP4=PIN_D0
//!   #PIN_SELECT CCP5=PIN_D3
//!   #PIN_SELECT CCP6=PIN_D2
//!   #PIN_SELECT CCP7=PIN_B3
//!   #PIN_SELECT CCP8=PIN_B2
//!   #PIN_SELECT CCP9=PIN_B4
//!   #PIN_SELECT CCP10=PIN_B5
//!   
   

//!   #USE PWM(stream=m11, output=PIN_D1, FREQUENCY=10kHz, DUTY=0)
//!   #USE PWM(stream=m12, output=PIN_D0, FREQUENCY=10kHz, DUTY=0)
//!   #USE PWM(stream=m21, output=PIN_D3, FREQUENCY=10kHz, DUTY=0)
//!   #USE PWM(stream=m22, output=PIN_D2, FREQUENCY=10kHz, DUTY=0)
//!   
//!   #USE PWM(stream=m31, output=PIN_B3, FREQUENCY=10kHz, DUTY=0)
//!   #USE PWM(stream=m32, output=PIN_B2, FREQUENCY=10kHz, DUTY=0)
//!   #USE PWM(stream=m41, output=PIN_B4, FREQUENCY=10kHz, DUTY=0)
//!   #USE PWM(stream=m42, output=PIN_B5, FREQUENCY=10kHz, DUTY=0)
   


#endif




#byte OSCTUNE = 0xF9B
#bit PLLEN=OSCTUNE.6


#byte INTCON2 = 0xFF1
#bit RBPU=INTCON2.7   // Port B internal pull-up enable bit (0=enable)


                         


#include <gogo_hid.h>
#include <rpt10.H>
//#include <bootload.h>

//#use rs232(baud=9600,UART1)
#use i2c(master, I2C1, FORCE_HW)
#use rs232(baud=115200, UART1, ERRORS)


#include <logovm.h>
#include <stdlib.H>
#include <i2cDisplay.h>
#include <i2c.h>   // i2c read/write routines
#include <ds1307.h>   // real time clock module
#include <7segment.h> // on-baord 7-segment display driver
#include <raspberrypi.h>  // raspberry-pi specific routines



#use fast_io(A)
#use fast_io(B)
#use fast_io(C)  
#use fast_io(D)
#use fast_io(E)
#use fast_io(F)
#use fast_io(G)


#define HARDWARE_ID1     0x02   // 02 = GoGo Board 5.x (formally the Pi Topping board)
#define HARDWARE_ID2     0x51   // 51 = version 5.1
#define FIRMWARE_ID      42    // Firmware version 

#define REGISTER_SIZE    63   // device register size

#define BEEP_DURATION   20   // how long a beep should take

#define defaultPort      0

#define channelSwitchDelay   100   // delay time in us after switching adc channels
                              // Don't decrease this value without testing.
                              // If the delay is too short (i.e. 10us) the adc won't
                              // have enough time to stabilize before reading the
                              // next channel.

#define STATE_CHANGE_BUFFER_SIZE   16   // size of buffer used by the if-state-change statement

/// Timer 1 Counter
/// This value should be 3036 ( 65536 - 0.1/(4/20000000) * 8)
/// where 0.1 is the time in seconds we want the ISR to be called
/// 4/20000000 is the time each counter takes to tick
/// 8 is the timer divider ... see setup_timer_1()

//#define T1_COUNTER      3036     // tick rate = 1.60/8 uSec (value according to the math)



#ifdef PIC18F66J50
   #define T0_COUNTER      56161      // at 48Mhz, and prescale=128 the interrupt period is
#endif

#ifdef PIC18F66J94
   #define T0_COUNTER      53036      // at 64Mhz, and prescale=128 the interrupt period is
#endif                                // calculated from 1/48 * 4 * 128 * (65536-53036)= 100 ms

#define PWM_PERIOD      27135      // 65535-38400. Used in Timer1, which gives a 25.6 ms period
#define LOGO_RAM_CACHE_SIZE   768   // size of the Logo binary code cache (in bytes).


#define  CMD_TIMEOUT_PERIOD  4     // determins how long befor the board will reset
                                    // the command state. Units in 1/10 of a second


// RESERVED_MEMORY_START must be the same as RECORD_BASE_ADDRESS in memoryMap.h
#define RESERVED_MEMORY_START    0x6B00 // must be a multiple of getenv("FLASH_ERASE_SIZE"), which is 1024 in the pic18F66J50
#define RESERVED_MEMORY_END      0xFFEF

#if defined(__PCM__)
   // reserve memory for the bootloader. This allows firmware updates via the serial port
   #org 0x1d00, 0x1FFF void loader16F876(void) {}
   
   // This memory area is used to store Cricket Logo commands
   #org 0x1c00, 0x1cff void cricketLogoMemoryArea(void) {}

#elif defined(__PCH__)
   // reserve memory for the bootloader. This allows firmware updates via the serial port
///   #org 0x6500, 0x7FFF void loader16F876(void) {}  // these are byte addresses. 
                                                   // equiv word addresses are 3D00 - 3FFF
   
   // This memory area is used to store Cricket Logo commands
///   #org 0x7200, 0x79DF void cricketLogoMemoryArea(void) {} // equiv word addresses are 3900 - 3DEF
      #org RESERVED_MEMORY_START, RESERVED_MEMORY_END void reserved(void) {} 
                                                 // do not allow gogo code to use the upper parts of
                                                 // the flash memory. They are reserved for data logging
                                                 // array storage, and the logo code. See memoryMap.h
                                                 // for memory allocation details.


#endif



/////////////////////////////////////////////////////////////////
//  Function Declaration
/////////////////////////////////////////////////////////////////

void startStopLogoProcedures(void); 
void stopLogoProcedures(void);


void setHigh(IOPointer Pin);
void setLow(IOPointer Pin);
int  readPin(IOPointer Pin);

short getBit(int InByte, int BitNo);
void setBit(int *InByte, int BitNo);
void clearBit(int *InByte, int BitNo);

void Ping(int Param);
void TalkToMotor(int MotorBits);
void MotorControl(int MotorCmd);
void SetMotorPower(int Power);
void setPWMduty(int duty, int1 mode=MOTOR_SERVO);
void ChangeMotorPower(int delta);
void createPWMVectorTable(void);
//void sortMtrDuty();
void SetMotorMode(int motorMode); // normal or servo

void ENHigh(int groupNo);
void ENLow(int groupNo);

void MotorON(int MotorNo);
void MotorOFF(int MotorNo);
void MotorRD(int MotorNo);
void MotorThisWay(int MotorNo);
void MotorThatWay(int MotorNo);
void MotorCoast(int MotorNo);
void miscControl(int cur_param, int cur_ext, int cur_ext_byte);

void beep();

void SetBurstMode(int SensorBits, int Mode);
void DoSensorStuff();
// this prints the result back to the PC
//#inline int16 outputSensor(int Target, int readMode);
// this one just returns the sensor value
unsigned int16 readSensor(int sensorNo);
long getSensorVal();
void switchAdcChannel(int channelNo);

void ProcessInput();
void ProcessRFInput();
//int  process_input();
//void Add_to_CMD(int InByte);
//void EndCMD();
//byte get_cmd_buff(int *cmd_ptr);
void init_variables();
void intro ();
void Halt();
void initBoard();

void DoDisplayModuleStuff();
void DoMotorStuff();

//////////////////////////////////////////
// Flash memory routines
/////////////////////////////////////////

void FLASHSetByteAddress(int16 address);
void FLASHBufferedWrite(int16 InByte) ;
void FLASHFlushBuffer();
void FLASHWrite(int16);
void writeFLASH(int16 memoryBlockAddress, int16 positionInMemoryBlock, int16 len, int *Buffer);

void cacheByteCode();

timer2ISR();

/////////////////////////////////////////////////////////////////
//  Global Variables
/////////////////////////////////////////////////////////////////

int gblByteCodeBuffer[LOGO_RAM_CACHE_SIZE];   // stores the user code in RAM for faster execution

int gblDeviceRegister[REGISTER_SIZE] = {0};
int gblHIDMessage[REGISTER_SIZE] = {0};
int gblI2CBuffer[REGISTER_SIZE] = {0};
int gblHIDMessageIndex = 2;  // init to 2 since location 0 = packet type, 1 = length

//IOPointer  MotorENPins [MotorCount]={  MTR1_EN, MTR2_EN, MTR3_EN, MTR4_EN};
IOPointer  MotorCWPins [MotorCount]={  MTR1_CW, MTR2_CW, MTR3_CW, MTR4_CW};
IOPointer  MotorCCPins [MotorCount]={  MTR1_CC, MTR2_CC, MTR3_CC, MTR4_CC};

unsigned int CMD_STATE;

int gbl_cur_cmd, gbl_cur_param, gbl_cur_ext, gbl_cur_ext_byte;
int gblExtCMDBuffer[32];   // buffer to hold the gogo extended command stream
int1 gblExtCmdMode=0;   // flag. Tells us if the command recieved is an extended command.
int gblExtCmdLen;
int gblExtCmdBufferIndex;


int gblSensorPortMap[8] = {11,10,7,3,2,1,0,4}; // this array maps the sensor number to the 
                                            // physical sensor port used on the PIC

int gblBurstModeBits;
int gblBurstModeCounter=0;   // tracks which sensor is the current burst mode sensor

int1 gblSlowBurstMode=0;  // determinds which burst mode we're in (0=normal, 1=slow)
                          // as of GoGo 4.0, this option has no effect. 
                          
int1 gblBurstModeTimerHasTicked=0;  // ticks every 1/72 sec (by timer0)
int gblCurSensorChannel;




int gblMotorMode=0b00000000;   // default to normal mode
int gblActiveMotors;
int gblMotorDir=0;
int gblMotorONOFF = 0;
int gblMtrDuty[MotorCount+1] = {0xff,0xff,0xff,0xff,0xff};  // Motor PWM Duty cycle
int gblMtrPrevDuty[MotorCount+1] = {0xff,0xff,0xff,0xff,0xff};  //Prev PWM Duty cycle -> to detect change 
int1 gblMtrNeedToRecreateMtrDutyVector = 0;  // True when gblMtrDuty != gblMtrPrevDuty
int1 gblAtEndOfPWMCycle = 0; // True when at end of a PWM cycle. This is when we can
                             // update the duty vector 
//int gblMtrDutyIndex[MotorCount+1] = {0,1,2,3,4}; // index to the sorted gblMtrDuty[]
//unsigned int gblTimer0Counter = MotorCount; // Motor duty cycle counter.
//unsigned int gblDutyCycleFlag = 0; // used to find the next duty cycle in timer0
//unsigned int gblCurrentDutyIndex = 0; // keeps track of the current duty cycle being used.

int1 flgNeedToTurnOffAllMotors=0; // flag to tell the main loop to turn off all motors

//////////////////////////////////////////////////////////
// PWM variables
//////////////////////////////////////////////////////////
int16 gblPWMVector[4] = {0,0,0,0};
int   gblPWMVectorPorts[4] = {0,0,0,0};
int   gblPWMVectorLen = 0;
int16 gblPWMTerminatingVector = PWM_PERIOD;
int   gblPWMVectorIndexCounter = 0;




// These two variables are for the NEWSERIAL, SERIAL commands in the Logo VM
unsigned char gblMostRecentlyReceivedByte;
int1 gblNewByteHasArrivedFlag = 0;



int1 gblLogoIsRunning = 0;     // flags if logo procedures are runing
int1 gblButtonPressed = 0;    // flags when the run button is pressed
int1 gblBtn1AlreadyPressed = 0;
unsigned int16 gblWaitCounter =0;  // used for the wait cmd in Logo vm
unsigned int16 gblCmdDelayCounter =0;  // used for the wait cmd in Logo vm


unsigned int16 gblTimer = 0;   // This is the timer for the TIMER and RESETT commands
unsigned int16 gblTickTimer = 0;  // this is a tick timer 
unsigned int16 gblTickCounter = 0;   // This is the tiker timer
unsigned int16 gblTickPeriod = 10;   // This defins how many 0.1 sec befor increasing the TickCounter


int gblCmdTimeOut = 0; // counter to make sure the command state is not stuck somewhere

//!int gblSerialBuffer[SERIAL_BUFFER_SIZE];
//!int gblSerialBufferPutIndex=0;
//!int gblSerialBufferGetIndex=0;
//!int gblSerialBufferIsFull=FALSE;

//////////////////////////////////////////////////////////
// I2C add-on display module variables
//////////////////////////////////////////////////////////

int gblTimeToProbeDisplayModule = 0;
int1 gblDisplayPresent = 0;
int gblTimeToSendSensorValues = 0;
int gblDisplayAddress = 0xb4;   // default to the 16x2 LED display
int gblDisplayAddressList[4] = {DISPLAY_7SEG_ADDR_1, DISPLAY_7SEG_ADDR_2, DISPLAY_LCD_ADDR_1, DISPLAY_LCD_ADDR_2}; // All the possible display addresses
                                                // 0xB0 = 7-segment #1
                                                // 0xB2 = 7-segment #2
                                                // 0xB4 = LCD #1
                                                // 0xB6 = LCD #2
int1 gblAutoDetectDisplays = 1; // flag indication whether or not we should auto detect displays
                                // This flag will be off when a user explicitly addresses a 
                                // display module in the Logo code. It will be reset only after
                                // a re-boot.

int1 gblI2CisBusy = 0;

//////////////////////////////////////////////////////////
// IR variables
//////////////////////////////////////////////////////////

int16 gblIRCode = 0;       // holds the most recently recived IR Code
int16 gblIRCodeTemp = 0 ;  // work variable
int1 gblCCP2_FE = 1;       // flag to determine a Falling Edge event
int16 gblREPulseTimeStamp=0;
int gblIRPulseCount = 0;  // identifies the current pulse #
//int16 gblIRLog[13] = {0};
int1 gblIRActivity = 1;  // indicate that there has been an IR activity.
                         // Timer3's ISR uses this flag to determine when
                         // an IR transmission is complete
int16 gblPrevIR;         // records the prev IR code
int IRThreashold=0;   
int1 gblNewIRCodeHasArrivedFlag=0;  // flag used in the Logo VM

//int1 gblNeedToBeep=0;   // flag telling the main loop to beep. Use to indicate
//// when logo procedure download is done.

int gblNeedToStartABeep = 0;  // timer0 will sound the beeper when flag set


int1 gblPauseI2CActivity = 0;  // when true-> all display i2c activity is paused

int  gblBeepDurationCounter=0;
int1 gblReadyToSendHIDPacket = 0;
int gblReadyToSendRpiHIDPacket = 0;
int1 gblReadyToSendI2CPacket = 0;

#if defined(__PCH__)
int gblFlashBuffer[getenv("FLASH_WRITE_SIZE")]; // buffer for flash write operations
int16 gblFlashOffsetIndex=0; // where in the flash block to write 
int16 gblFlashBaseAddress; // where the flash buffer shuld be written to in the flash mem
int16 gblFlashBufferCounter = 0;  // counts the number of bytes to write
#endif

int gblCommChannel=COMM_USB;


int16 gblCCPtest[16];
int gblCCPindex=0;
int1 gblPrintCCPbuffer=0;

int gblStateChangeFlags[STATE_CHANGE_BUFFER_SIZE]={0};  // each bit is used to track state changes used
                                                    // in the if-state-change statement. 

/////////////////////////////////////////////////////////////////////////
//
//   I N T E R R U P T    S E R V I C E    R O U T I N E S
//
/////////////////////////////////////////////////////////////////////////








// Peroiod = 0.1 sec
#int_timer0                         
void rtccISR() {                      


      set_timer0(T0_COUNTER);  // this is to keep the interrupt period constant 

      gblTimer++;  // increase the global timer
      
      gblTickTimer++;
      if (gblTickTimer == gblTickPeriod) {
         gblTickCounter++; 
         gblTickTimer=0;
      }

      // the Wait opcode sets gblWaitCounter,
      // then waits until it gets to 0.
      if (gblWaitCounter > 0) { gblWaitCounter--; }


      ////////////////////////////////////////////
      // check menu button (button 1)
      // if it is pressed
      if (input(RUN_BUTTON) == 0) {
            // if button not already pressed
            if ( gblBtn1AlreadyPressed) {
               // do nothing if button press has been processed before
            } else {
               gblButtonPressed = !gblButtonPressed;
               gblBtn1AlreadyPressed=1;
               
               startStopLogoProcedures();
               
              ////////////////////////////////////////////////////////

            } 
      } else if (gblBtn1AlreadyPressed) {
         gblBtn1AlreadyPressed=0;
      }

      // tells main() to check if the display module is connected every 1 second
      gblTimeToProbeDisplayModule++;                               

      check7SegmentButton();   // check the 7-segment button
      update7SegementBuffer();  // update the 7-segment display buffer

      if (gblRpiWatchDog < RPI_WATCH_DOG_LIMIT){
         gblRpiWatchDog++;  // increase the Raspberry Pi watchdog timer
      }

}


// Timer1 contains the motor PWM signal algorithm

// Period is dynamic depending on the PWM settings
#int_timer1
void timer1ISR() {

   int associatedPorts;
   int i;
   
   ////////////////////////////////////////////////////////////////////////////
   // If this interrupt has taken place at the end of a PWM pulse. Thus, we need
   // to put these pulses "low" 
   ////////////////////////////////////////////////////////////////////////////
   
   if (gblPWMVectorIndexCounter < gblPWMVectorLen) {
      // cache array val in a normal variable
      associatedPorts = gblPWMVectorPorts[gblPWMVectorIndexCounter];
      for (i=0 ; i<MotorCount ; i++) {
         if (getBit(associatedPorts, i)) {
            if (getBit(gblMotorONOFF, i) == ON) {
               if (getBit(gblMotorMode, i) == MOTOR_NORMAL) {
                     setLow(MotorCWPins[i]);
                     setLow(MotorCCPins[i]);
               }
               // this is the servo mode
               else
                  setLow(MotorCCPins[i]);
            }
         }
      }
      
      ///////////////////////////////////////////////
      // setup for the next interrupt
      ///////////////////////////////////////////////
      if (++gblPWMVectorIndexCounter < gblPWMVectorLen) {
         set_timer1(gblPWMVector[gblPWMVectorIndexCounter]);
      } else {
         set_timer1(gblPWMTerminatingVector);
         gblAtEndOfPWMCycle = 1;  // notify main loop that we have reached the end
                                  // of a PWM cycle. It can make changes to the 
                                  // PWM vectors if needed (without disrupting 
                                  // the PWM pulses)
      }
      
   } 

   /////////////////////////////////////////////////////////////
   //  Else then this interrupt is at the beginning of a new PWM pulse
   //  Thus, we need to put all the pulses "high"
   /////////////////////////////////////////////////////////////


   else {
         gblAtEndOfPWMCycle = 0;

         for (i=0 ; i<MotorCount ; i++)  {
            if (getBit(gblMotorONOFF, i) == ON) {
               if (gblMtrDuty[i] > 0) {
                  if (getBit(gblMotorMode, i) == MOTOR_NORMAL) {
                     if (getBit(gblMotorDir, i)) {
                        setHigh(MotorCWPins[i]);
                        setLow(MotorCCPins[i]);
                     } else {
                        setHigh(MotorCCPins[i]);
                        setLow(MotorCWPins[i]);
                     }
                  }
                  // this is the servo mode
                  else {
                     setHigh(MotorCCPins[i]);  // this is the servo pulse pin
                     setHigh(MotorCWPins[i]);  // this is the servo powor pin
                  }
               } 
               // force both CC, CW pins low if power is 0
               // this is just in case something has accidentally
               // cased any pin to go high
               else {
                  setLow(MotorCWPins[i]);
                  setLow(MotorCCPins[i]);
               }
            } else {  // if the motor is off -> make sure the pins are low
                  setLow(MotorCWPins[i]);
                  setLow(MotorCCPins[i]);
            }                
         }
      
      
      if (gblPWMVectorLen > 0) {

         ///////////////////////////////////////////////
         // setup for the next interrupt
         //////////////////////////////////////////////
         gblPWMVectorIndexCounter = 0;
         set_timer1(gblPWMVector[0]);
      
     
      } else {  // PWM is not in use -> keep interrupt rate = PWM Period
         set_timer1(PWM_PERIOD);
         gblAtEndOfPWMCycle = 1; // needed to make this flag work when no PWM is active
      }


   }

}




// timer2 is neccessary for the PIC's hardware PWM (which drives the beeper)
// see setup_timer2() for the period calculation
#int_timer2
void timer2ISR() {



   gblBurstModeTimerHasTicked=1;  // this triggers the burst mode to send data. See DoSensorStuff()

   gblTimeToSendSensorValues++; // signal main() to send sensor data to the diaply module

   if (gblCmdDelayCounter > 0) {
      gblCmdDelayCounter--;  // used to create a delay between some Logo commands 
                             // for better system stability 
   }

   if (gblNeedToStartABeep) {
      if (gblBeepDurationCounter++ == 0) {
          set_pwm1_duty(50);        // make a beep
      } else if (gblBeepDurationCounter == BEEP_DURATION) {    
         set_pwm1_duty(0);          // stop the beep
         gblBeepDurationCounter = 0;
         gblNeedToStartABeep = 0;
      }
   } 
   

   showNextDigit();   // update the built-in 7-segment display
}

// timer3 interrupt is fired only when no IR pulses have been received (end of transmission)
#int_timer3
void timer3ISR() {
   
   // if no IR Activity in the past timer3 period -> assume IR data is done
   // Also make sure the received data is 12-bit long (SONY code length).
   if (!gblIRActivity ) {
      // only update if a new value has arrived
      if ((gblIRCodeTemp != 0) && (gblIRPulseCount == 12)) {
         if (gblIRCode != gblIRCodeTemp) {
            
            if (gblIRCodeTemp == 149 ) {   // ON/OFF button -> run/stop Logo procedures
               startStopLogoProcedures();
            } else {      
               gblIRCode = gblIRCodeTemp;
               gblNewIRCodeHasArrivedFlag = 1;  // this flag is used in the Logo VM
            }

            IRThreashold = 0; 
            gblIRCodeTemp = 0;
         }
      }

      // if time has passed with no activity -> reset gblPrevIRCode so that it
      // recognizes any incoming IR Code as a new event (even if it is the same
      // code as before).
      if (IRThreashold++ > 50) {
         gblIRCode = 0;
         IRThreashold = 0;
      }

   } 
   
   gblIRActivity = 0;
}


#int_ccp2
void ccp2_isr() { 

//  Captures IR pulses
//
//  For a SONY remote:
//
//  Logic 0 = 0.6ms high + 0.6ms low
//  Logic 1 = 1.2ms high + 0.6ms low
//  Start Bit = 2.4ms high + 0.6ms low
//
//  Note that the IR sensor on the board inverts the logic above!
//
//  commands are transmitted every 45 ms (from start to start) when the
//  remote button is held pressed.

//!   #DEFINE SHORT_PULSE_WIDTH     4000
//!   #DEFINE LOGIC_0_PULSE_WIDTH   6666
//!   #DEFINE LOGIC_1_PULSE_WIDTH  12000 
//!

#ifdef PIC18F66J50 
   #DEFINE SHORT_PULSE_WIDTH     3000
   #DEFINE LOGIC_0_PULSE_WIDTH   5000
   #DEFINE LOGIC_1_PULSE_WIDTH   9000 
#endif

#ifdef PIC18F66J90
   #DEFINE SHORT_PULSE_WIDTH     2000
   #DEFINE LOGIC_0_PULSE_WIDTH   4000
   #DEFINE LOGIC_1_PULSE_WIDTH   6000 
#endif  
  

   int16 pulseWidth;

   
   gblIRActivity = 1;  // indicate that there has been an IR activity.
                       // Timer3's ISR uses this flag to determine when
                       // an IR transmission is complete

   // If a falling edge event occured, We will timestamp the CCP_2 value
   // and configure CCP2 to interrupt again at the raising edge.
   if (gblCCP2_FE) {
      
      setup_ccp2( CCP_CAPTURE_RE);
      gblCCP2_FE = 0;
      
      gblREPulseTimeStamp = CCP_2;
      
   
   } else {
      // A rising edge event occured. We calculate the pulse width to
      // determine a bit 0 or bit 1. 

      setup_ccp2( CCP_CAPTURE_FE);
      gblCCP2_FE = 1;
   
      if (CCP_2 > gblREPulseTimeStamp)  // this is the normal case 
         pulseWidth = CCP_2 - gblREPulseTimeStamp;
      else  // this means time3 has wrapped around
         pulseWidth = 65535 - gblREPulseTimeStamp + CCP_2;
      
      //gblIRLog[gblIRPulseCount] = pulseWidth;
      
//!      gblCCPtest[gblCCPindex++] = pulseWidth;
//!      if (gblCCPindex > 12) {
//!         gblPrintCCPbuffer = 1;
//!      }

      if (pulseWidth < SHORT_PULSE_WIDTH) {
         // ignore short pulses -> probably noise
         gblIRCodeTemp = 0;
         gblIRPulseCount = 0;
        
        
      } else if (pulseWidth < LOGIC_0_PULSE_WIDTH) {  // Logic 0
                                       // theoretical pulse count is 4800 (0.6 ms)
                                       // actual pulses from test hovers just above 5333
         // just increase the counter
         gblIRPulseCount++;
          

      } else if (pulseWidth < LOGIC_1_PULSE_WIDTH) {  // Logic 1
                                       // theoretical pulse count is 9600 (1.2 ms)
                                       // actual pulses from test hovers around 10133
      
         // set the bit 
         gblIRCodeTemp |= (1 << gblIRPulseCount);
         gblIRPulseCount++;
         

      }   else {
         // a long pulse marks the beginning of a new IR transmission
         gblIRCodeTemp = 0;
         gblIRPulseCount = 0;
 //        gblCCPindex=0;
      }
      
   
   }



}

/*
#INT_LOWVOLT
void lowVoltISR() {
   stopLogoProcedures();
}
*/

void stopLogoProcedures(void) {

//      output_toggle(USER_LED);
//      disable_interrupts(GLOBAL);     // why do this??

      gblWaitCounter = 0;  // reset wait command (so the running Logo wait code
                           // stops immediately).
      gblONFORNeedsToFinish = 0; // incase an onfor is running.
      gblLogoIsRunning = 0;
      flgNeedToTurnOffAllMotors=1;  // tell the main loop to turn off the motors
      cls_internal7Seg();   // clear the internal 7-segment's screen
      
      output_low(RUN_LED);
}

void startLogoProcedures(void) {
      
      gblWaitCounter = 0;  // reset wait command (so the running Logo wait code
                           // stops immediately).
      gblONFORNeedsToFinish = 0; // incase an onfor is running.

      srand(gblTimer);  // seed for the random function;
      output_high(RUN_LED);

      gblMemPtr = 0;

      clearStack();
      gblNewByteHasArrivedFlag=0;
      gblLogoIsRunning = 1;
      gblErrFlag=0;
      gblForce7SegUpdate=1; // force the internal 7-segment screen to update

      // these are attmpts to clear the I2C state if there is a lockup
      resetI2C();
      output_float(I2C_SDA);
      output_float(I2C_SCL);
      
      cacheByteCode();   
   

}


void cacheByteCode() {
   int16 i;
   
   for (i=0;i<LOGO_RAM_CACHE_SIZE;i++) {
      gblByteCodeBuffer[i] = fetchNextOpcodefromFlash(i);
      output_toggle(USER_LED);
   }
   
}




void startStopLogoProcedures(void) {

 
      

     ////////////////////////////////////////////////////////
     // run Logo procedures
     if (!gblLogoIsRunning)
     {
         startLogoProcedures();       
      } else {  // stop Logo
         stopLogoProcedures();
      }

}



void setLow(IOPointer Pin)
{
//   *(Pin>>3) &= (~(1<<(Pin&7)));
   output_bit(Pin, 0);
}

void setHigh(IOPointer Pin)
{
//   *(Pin>>3) |= (1<<(Pin&7));
   output_bit(Pin, 1);

}


int readPin(IOPointer Pin)
{
//   return (*(Pin>>3) & (1<<(Pin&7))) ;
   return (input(Pin));

}



//////////////////////////////////////////////////
short getBit(int InByte, int BitNo)
{  return ((InByte >> BitNo) & 1);
}

void setBit(int *InByte, int BitNo)
{  *InByte |= (1<<BitNo);
}

void clearBit(int *InByte, int BitNo)
{  *InByte &= ~(1<<BitNo);
}


void active_comm_putc(char c) {
   if (gblCommChannel == COMM_SERIAL) {
      putc(c);
   }else if (gblCommChannel == COMM_USB) {
      //usb_cdc_putc(c);
      hid_putc(c);
   }
}

/////////////////////////////////////////////////////////////////////////
//
//   M O T O R   C O N T R O L
//
/////////////////////////////////////////////////////////////////////////

void TalkToMotor(int MotorBits)
{

   // Each bit represents one motor. i.e 0b00010010 means motor 2 and 5 are active
   gblDeviceRegister[REG_ACTIVE_MOTORS] = MotorBits;

//   printf("%c%c%c", ReplyHeader1, ReplyHeader2, ACK_BYTE);  // send acknowledge byte

}

void ToggleMotorSelection(int motorNumber) {
   if (bit_test(gblDeviceRegister[REG_ACTIVE_MOTORS], motorNumber)) {
      bit_clear(gblDeviceRegister[REG_ACTIVE_MOTORS], motorNumber);
   } else {
      bit_set(gblDeviceRegister[REG_ACTIVE_MOTORS], motorNumber);
   }


}


void MotorControl(int MotorCmd)
{
   int i;

   for (i=0;i<MotorCount;i++)
   {
      if ((gblDeviceRegister[REG_ACTIVE_MOTORS] >> i) & 1 )
      {
         SetMotorMode(MOTOR_NORMAL);

         switch (MotorCmd)
         {
         case MTR_ON:   MotorON(i);
               break;
         case MTR_OFF: MotorOFF(i);
               break;
         case MTR_RD: MotorRD(i);
               break;
         case MTR_THISWAY: MotorThisWay(i);
               break;
         case MTR_THATWAY: MotorThatWay(i);
               break;
         case MTR_COAST: MotorCoast(i);
               break;
         }

      }
   }
   

}

/////////////////////////////////////////////////
// Refer to the problem explained in MotorControl() if
// this function suddenly missbehave
void SetMotorPower(int Power)
{
   // Scale the user power range 0-100 to the hw PWM range 0-255
   Power = (int)(((float)Power / 100) * 255);

   setPWMduty(Power);


}

// define a alias 
#define setServoDuty(duty) setPWMduty(duty, MOTOR_SERVO)

//!void setServoDuty(int duty) {
//!
//!   int i;
//!
//!   for (i=0;i<MotorCount;i++)
//!   {
//!      if ((gblDeviceRegister[REG_ACTIVE_MOTORS] >> i) & 1 )
//!      {
//!         // if that port is not already in a servo mode
//!         if (((gblMotorMode >> i) & 1) == MOTOR_NORMAL) {
//!               MotorON(i);
//!               MotorThisWay(i);
//!               setBit(&gblMotorMode, i);  // set to servo mode
//!         }
//!
//!         gblMtrDuty[i] = duty;
//!         if (duty != gblMtrPrevDuty[i]) {
//!            gblMtrNeedToRecreateMtrDutyVector = 1; // Notify the main loop that we need to
//!                                                   // update the duty vector
//!            gblMtrPrevDuty[i] = duty;
//!         }
//!      }
//!   }
//!   
//!}
//!

void setPWMduty(int duty, int1 mode=MOTOR_NORMAL) {

   int i;

   for (i=0;i<MotorCount;i++)
   {
      if ((gblDeviceRegister[REG_ACTIVE_MOTORS] >> i) & 1 )
      {
         if (mode == MOTOR_SERVO) {
            // if that port is not already in a servo mode
            if (((gblMotorMode >> i) & 1) == MOTOR_NORMAL) {
                  MotorON(i);
                  MotorThisWay(i);
                  setBit(&gblMotorMode, i);  // set to MOTOR_SERVO mode
            }      
         }
         
         gblMtrDuty[i] = duty;
         if (duty != gblMtrPrevDuty[i]) {
            gblMtrNeedToRecreateMtrDutyVector = 1; // Notify the main loop that we need to
                                                   // update the duty vector
            gblMtrPrevDuty[i] = duty;
         }
      }
   }
   
}


void ChangeMotorPower(int delta) {
   int i;

   for (i=0;i<MotorCount;i++)
   {
      if ((gblDeviceRegister[REG_ACTIVE_MOTORS] >> i) & 1) {
         gblMtrDuty[i] = gblMtrDuty[i] + delta;
      }
   }

   //createPWMVectorTable();
   gblMtrNeedToRecreateMtrDutyVector = 1;
}



// createPWMVectorTable() will create an interrupt vector table. 
//
// The goal is for timer1 to interrupt at the particular time each pulse needs to 
// be set low. A table will be created containing the number of ticks to the next 
// interrupt. For example, here's a hypothetical situation:
//
// Motor A: PWM level = 255 (Full power)
// Motor B: PWM level = 100
// Motor C: PWM level = 50
// Motor D: PWM level = 100
//
// The vector table will look like this
//
//    int16 PWMVector[4] = [65535-(50*150), 65535-((100-50) * 150), 0, 0]
//    int PWMVectorPorts[4] = [0b0100, 0b1010, 0, 0]
//    int PWMVectorLen = 2
//    int16 PWMTerminatingVector = 65535 - (38400 - 100*150)
//
//    * The PWMVector array contains Timer1's counter value that would overflow 
//      (causing an interrupt) at the desired time
//    * The PWMVectorPorts contains bits that indicate which motor ports are 
//      associated with that power level.
//    * PWMVectorLen indicates how many vectors are present
//    * PWMTerminatingVector contains the number of ticks until then end of that 
//      PWM period (after the last pulse).
//
// Timer1 Settings
//
//    * The PWM period is 12.8ms or approximately 78Hz.
//    * This allows 256 levels at 0.05 ms pulse resolution.
//    * When Timer1 prescale is 4, the number of ticks for a 12.8ms period is 38400. 
//      Divide that by 256 pulse levels and you will get 150 ticks per level. 
//      For example, if we decide to widen a pulse by 10 steps (out of 256) we will 
//      need Timer1 to count 150x10 = 1500 ticks more before setting the pulse low.



void createPWMVectorTable(void) {

   int portsProcessed=0;  // each bit tracks which ports have been processed
   int i=0;
   int vectorIndex=0; // the current item in the PWM Vector Table
   int currentPowerLevel; // buffer to cache an array value (just to make exe faster)
   int currentMinLevel=255; // tracks the current min power level
   int portList=0; // each bit tracks the ports with the same power level
   int prevMinLevel=0; // remembers the previous min power level
   
   
   gblPWMVectorIndexCounter=0; // reset the PWM pulses

   // while not all ports have been processed
   while (portsProcessed != 0b1111) {

      for (i=0;i<MotorCount;i++) {
         
         // skip if this port has already been processed
         if (getBit(portsProcessed,i))
            continue;
         
         // cache the array value in a normal variable (it's faster)
         currentPowerLevel = gblMtrDuty[i]; 
   
         // don't create vector item for power levels 0, 255 
         if ((currentPowerLevel == 255) || (currentPowerLevel == 0)) {
            setBit(&portsProcessed,i);
            continue;
         }
   
         // look for the min power level
         if (currentPowerLevel < currentMinLevel) {
            currentMinLevel = currentPowerLevel;
            portList = 1 << i;   // remember which port this is
          
         // if found another port with equal min power level
         } else if (currentPowerLevel == currentMinLevel) {
            portList |= (1 << i);  // add the port to the list
         }   
      } 

      portsProcessed |= portList; // mark ports in the list as processed

      // if an unprocessed power level was found -> add it to the vector array

      if (currentMinLevel < 255) {      
         // We store the number of "Timer ticks" till the next timer interrupt
         gblPWMVector[vectorIndex] = 65535 - ((int16)(currentMinLevel-prevMinLevel) * 150);
         // identify the ports associated with this power level
         gblPWMVectorPorts[vectorIndex] = portList;
         
         // update working variables
         prevMinLevel = currentMinLevel;
         currentMinLevel = 255;   // reset the variable
         vectorIndex++;
      }
          
   }
   // Vector length
   gblPWMVectorLen = vectorIndex;
   
   if (vectorIndex > 0)
      // Time (ticks) from the last pulse to the end of the PWM period
      // equivalent to 65535 - (PWM_Period_Ticks - (prevMinLevel * 150))
      gblPWMTerminatingVector = PWM_PERIOD + ((int16)prevMinLevel*150);
   else
      gblPWMTerminatingVector = PWM_PERIOD;




}


// Set the mode of the active motors (NORMAL or SERVO)
void SetMotorMode(int motorMode) {
   int i;

   for (i=0;i<MotorCount;i++) {
      if ((gblDeviceRegister[REG_ACTIVE_MOTORS] >> i) & 1)
         if (motorMode == MOTOR_NORMAL)
            clearBit(&gblMotorMode, i);
         else // Servo mode
            setBit(&gblMotorMode, i);

   }
}


// Sets "both" the EN pins of a h-bridge chip
void ENHigh(int groupNo) {
   groupNo<<=1;

//   setHigh(MotorENPins[groupNo]);
//   setHigh(MotorENPins[groupNo+1]);



   // power on the motor chip
   if (!groupNo) {
      output_high(MOTOR_AB_EN);
   } else {
      output_high(MOTOR_CD_EN);
   }

}


// Clears "both" the EN pins of a h-bridge chip
// but do it only if both motors on the chip
// is in the coast state
void ENLow(int groupNo) {
   groupNo<<=1;

//   setLow(MotorENPins[groupNo]);
//   setLow(MotorENPins[groupNo+1]);

   // power off the motor chip
   if (!groupNo) {
      output_low(MOTOR_AB_EN);
   } else {
      output_low(MOTOR_CD_EN);
   }


}



void MotorON(int MotorNo)
{

// no need to directly output to pins here. the PWM routine in Timer1
// will take care of it asynchronously. Only update the ONOFF flags
// -Roger 30 June 2012. Firmware v13.

//!   IOPointer MtrCC, MtrCW;
//!
//!   MtrCW       = MotorCWPins[MotorNo];
//!   MtrCC       = MotorCCPins[MotorNo];
//!
//!   // if power level is 0 -> don't turn on the motor
//!   if (gblMtrDuty[MotorNo] > 0) {
//!      if (getBit(gblMotorDir,MotorNo))
//!           {    setLow(MtrCC); setHigh(MtrCW);   }
//!      else
//!         {   setHigh(MtrCC); setLow(MtrCW);   }
//!   }
   
   // In gogo board 2.3 and later, we have to set both the EN pins on the h-bridge
   // to turn on one motor port. That is
   //  Motor 1 needs EN1 and EN2 to be high
   //  Motor 2 needs the same as motor 1
   //  Motor 3 needs EN3 and EN4 to be high
   //  Motor 4 needs the same as motor 3
   //
   //  ENHigh handles this.
   ENHigh(MotorNo>>1);

   setBit(&gblMotorONOFF,MotorNo);
   
}



void MotorOFF(int MotorNo)
{

   IOPointer MtrCC, MtrCW;

   MtrCW       = MotorCWPins[MotorNo];
   MtrCC       = MotorCCPins[MotorNo];

   
   setHigh(MtrCC);
   setHigh(MtrCW);

   clearBit(&gblMotorONOFF,MotorNo);
   ENHigh(MotorNo>>1);     // this ensures the motor breaks. Sometimes the timer_isr turns off the enable pin, which turns 'stop' into 'coast'

}



void MotorRD(int MotorNo)
{
// no need to directly output to pins here. the PWM routine in Timer1
// will take care of it asynchronously. Only update the Motor direction flags
// -Roger 30 June 2012. Firmware v13.

//!   IOPointer MtrCC, MtrCW;
//!
//!    MtrCW       = MotorCWPins[MotorNo];
//!    MtrCC       = MotorCCPins[MotorNo];
//!
//!      if (getBit(gblMotorDir,MotorNo))
//!      {   if (getBit(gblMotorONOFF, MotorNo)) {
//!            setLow(MtrCW);
//!            setHigh(MtrCC);
//!         }
//!         clearBit(&gblMotorDir,MotorNo);
//!      } else
//!      {   if (getBit(gblMotorONOFF, MotorNo)) {
//!            setHigh(MtrCW);
//!            setLow(MtrCC);
//!         }
//!         setBit(&gblMotorDir,MotorNo);
//!      }
//!

      if (getBit(gblMotorDir,MotorNo))
      {   clearBit(&gblMotorDir,MotorNo);
      } else
      {   setBit(&gblMotorDir,MotorNo);
      }


}


///////////////////////////////////////////////////////////////////


void MotorThisWay(int MotorNo)
{   
// no need to directly output to pins here. the PWM routine in Timer1
// will take care of it asynchronously. Only update the Motor direction flags
// -Roger 30 June 2012. Firmware v13.

//!   IOPointer MtrCC, MtrCW;
//!
//!     MtrCW       = MotorCWPins[MotorNo];
//!     MtrCC       = MotorCCPins[MotorNo];
//!
//!     setBit(&gblMotorDir,MotorNo);
//!
//!      if (getBit(gblMotorONOFF, MotorNo)) {
//!          setLow(MtrCC);
//!            setHigh(MtrCW);
//!      }
//!
     setBit(&gblMotorDir,MotorNo);


}


void MotorThatWay(int MotorNo)
{   
// no need to directly output to pins here. the PWM routine in Timer1
// will take care of it asynchronously. Only update the Motor direction flags
// -Roger 30 June 2012. Firmware v13.

//!   IOPointer MtrCC, MtrCW;
//!
//!     MtrCW       = MotorCWPins[MotorNo];
//!     MtrCC       = MotorCCPins[MotorNo];
//!
//!     clearBit(&gblMotorDir,MotorNo);
//!
//!      if (getBit(gblMotorONOFF, MotorNo)) {
//!         setLow(MtrCW);
//!           setHigh(MtrCC);
//!      }
//!
   clearBit(&gblMotorDir,MotorNo);

}


void MotorCoast(int MotorNo)
{
// no need to directly output to pins here. the PWM routine in Timer1
// will take care of it asynchronously. Only update the ONOFF flags
// -Roger 30 June 2012. Firmware v13.

//!   IOPointer MtrCC, MtrCW;
//!
//!     MtrCW       = MotorCWPins[MotorNo];
//!     MtrCC       = MotorCCPins[MotorNo];
//!
//!        clearBit(&gblMotorONOFF,MotorNo);
//!
//!      setLow(MtrCW);
//!      setLow(MtrCC);
//!

   clearBit(&gblMotorONOFF,MotorNo);


   // if both ports on the h-bridge is off then turn off the
   // EN pins
   if (  !((gblMotorONOFF >> (MotorNo & 0b10)) & 1) &&
         !((gblMotorONOFF >> ((MotorNo & 0b10) + 1)) & 1) ) {
            ENLow(MotorNo>>1);
         }
}

void DoMotorStuff() {
   
   // Update the motor duty vector if needed.
   // Do this only at the end of a PWM cycle so that we don't disrupt
   // the current PWM generation process.
   
   if (gblAtEndOfPWMCycle) {
      if (gblMtrNeedToRecreateMtrDutyVector) {
         createPWMVectorTable();
         gblMtrNeedToRecreateMtrDutyVector = 0;

      }
   }

}



///////////////////////////////////////////////////////////
//
//    M i s c   C o n t r o l
//
///////////////////////////////////////////////////////////

void miscControl(int cur_param, int cur_ext, int cur_ext_byte)
{
//   int16 counter;

   switch (cur_param) {
      case MISC_USER_LED:
         if (cur_ext == TURN_USER_LED_ON) { USER_LED_ON; }
         else {USER_LED_OFF; }
         break;
      case MISC_BEEP:
         beep();
         break;
      case MISC_SET_PWM:
         MotorControl(MTR_ON);
         MotorControl(MTR_THISWAY);
         SetMotorMode(MOTOR_SERVO);
         SetMotorPower(cur_ext_byte);
         break;

// we handle EEPROM upload in the main loop (urr .. ugly code)
      case MISC_UPLOAD_EEPROM:

         break;

      case MISC_I2C_SETUP:
         switch (cur_ext) {
            case I2C_START:
               gblI2CisBusy = 1;  // reserve the i2c bus
               i2c_start();
               break;
            case I2C_STOP:
               i2c_stop();
               gblI2CisBusy = 0;  // release the i2c bus
               break;
            case I2C_WRITE:
               i2c_write(cur_ext_byte);
//               printf("%c", cur_ext_byte);
               break;
            case I2C_READ:
               i2c_read(0);
               break;
            }
         break;

   case MISC_AUTORUN_CONFIG:
      switch(cur_ext_byte) {
         case AUTORUN_ON:
             FLASHSetByteAddress(AUTORUN_STATUS_ADDRESS);
             FLASHWrite(AUTORUN_ON);
             break;
         case AUTORUN_OFF:
             FLASHSetByteAddress(AUTORUN_STATUS_ADDRESS);
             FLASHWrite(AUTORUN_OFF);
             break;
      }
      break;
   }
}


/////////////////////////
//    User LED controls

void uLED_on()
{  output_high(USER_LED);
}

void uLED_off()
{  output_low(USER_LED);
}


///////////////////////
//   Beep

void beep() {
   // this flag causes timer0 to sound the beeper
   gblNeedToStartABeep = BEEP_DURATION;

}



void setAutorunState(int state) {
   int i;
   if (state) {
      FLASHSetByteAddress(AUTORUN_STATUS_ADDRESS);
      FLASHWrite(AUTORUN_ON);
   
   } else {
      FLASHSetByteAddress(AUTORUN_STATUS_ADDRESS);
      FLASHWrite(AUTORUN_OFF);
   
   }
}



// update the device register values
void DoDeviceRegisterStuff() {
   gblDeviceRegister[REG_MOTOR_ON_OFF_STATUS] = gblMotorONOFF;
   gblDeviceRegister[REG_MOTOR_DIRECTION] = gblMotorDir;
   gblDeviceRegister[REG_MOTOR_A_DUTY] = gblMtrDuty[0];
   gblDeviceRegister[REG_MOTOR_B_DUTY] = gblMtrDuty[1];
   gblDeviceRegister[REG_MOTOR_C_DUTY] = gblMtrDuty[2];
   gblDeviceRegister[REG_MOTOR_D_DUTY] = gblMtrDuty[3];
   
   gblDeviceRegister[REG_IR_VALUE] = gblIRCode;
   gblDeviceRegister[REG_USER_LED_STATUS] = input(USER_LED);
   gblDeviceRegister[REG_BUTTON_STATUS] = input(RUN_BUTTON);


}

/////////////////////////////////////////////////////////////////////////
//
//   P R O C E S S   S E N S O R    W H I L E    I D L E
//
/////////////////////////////////////////////////////////////////////////



void DoSensorStuff()
{
//   int i;
   int16 sensorVal;
   

   //////////////////////////////////////////////////////////////////
   //
   //  Burst sensor data to computer

   if (gblBurstModeTimerHasTicked) {

      sensorVal = read_adc();
      
      gblDeviceRegister[REG_SENSOR1_HIGH + (gblBurstModeCounter<<1)] = sensorVal>>8;
      gblDeviceRegister[REG_SENSOR1_HIGH + (gblBurstModeCounter<<1) + 1] = sensorVal&0xff;

      // gblBurstModeCounter will circulate from 0 - 7
      gblBurstModeCounter = (gblBurstModeCounter+1) % 8;
      
      if (gblBurstModeCounter == 0) {
         gblReadyToSendHIDPacket = 1;
      } else if (gblBurstModeCounter == 4) {
         gblReadyToSendRpiHIDPacket = 1;
      }
      
      // we don't add the ADC channel switch delay because this procedure
      // will execute at the same rate as timer2's period, which is 2.67 ms
      // so there is plenty of delay time there already.
      set_adc_channel(gblSensorPortMap[gblBurstModeCounter]);


//!      // if this sensor is in Burst mode -> send value to computer
//!      if ((gblBurstModeBits>>gblBurstModeCounter) & 1)
//!      {
//!          SensorVal=readSensor(gblBurstModeCounter);
//!          printf(hid_putc, "%c%c%c", 0x0c, (gblBurstModeCounter << 5) | (SensorVal >> 8), SensorVal & 0xff);
//!      }

      
      gblBurstModeTimerHasTicked = 0;
    }




}


/////////////////////////////////////////////////////////////////////////
//
//   S E N S O R   C O N T R O L
//
/////////////////////////////////////////////////////////////////////////


void SetBurstMode(int SensorBits, int Mode)
{

   gblBurstModeBits = SensorBits;
   
   // As of gogo 4.0 there are no more slow/fast modes. 
   // Setting this here has no effect.
   if (Mode > 0)
      gblSlowBurstMode = 1; // switch to SLOW burst mode.
   else
      gblSlowBurstMode = 0; // switch to normal (fast) burst mode.


}


unsigned int16 readSensor(int sensorNo) {
   int16 temp;
   
   temp = gblDeviceRegister[REG_SENSOR1_HIGH + (sensorNo<<1)] << 8;
   temp += gblDeviceRegister[REG_SENSOR1_HIGH + (sensorNo<<1) + 1];

   return(temp);
//!
//!      if (gblCurSensorChannel != sensorNo)
//!      {   switchAdcChannel(sensorNo);
//!           gblCurSensorChannel=sensorNo;
//!      }
//!      return(getSensorVal());
}



//!/////////////////////////////////////////////////
//!long getSensorVal()
//!{
//!   long sensorVal;
//!
//!   delay_us(channelSwitchDelay);   // wait for adc to stabilize
//!                                   // Although this so often unneeded and can
//!                                   // slow down the execution, we are playing
//!                                   // it
//!   sensorVal=read_adc();
//!   //return (sensorVal >> 6);  // use this line if using PIC-C compiler version 2.x
//!
//!   // the PIC 16F77 ADC is only 8 bits. So, we simulate 10 bits by shifting
//!   // left 2 bits. But why in the code we shif right 6 bits? This is because
//!   // somehow the 8 bit sensor readings are stored in the higher byte
//!   // of the long variable. So, we have to shift right 8 bits before shifting
//!   // left. So, 8-2 is 6.
//!   #IFDEF PIC16F77
//!   sensorVal >>= 6;
//!   #ENDIF
//!
//!   return (sensorVal);        // this line works with PIC-C compiler version 3.x
//!
//!}
//!
//!/////////////////////////////////////////////////
//!void switchAdcChannel(int channelNo) {
//!   set_adc_channel(channelNo);
//!}


//!//////////////////////////////////////////////////////////////////////////////
//!// 
//!// Fetch a character from the serial buffer
//!//
//!//////////////////////////////////////////////////////////////////////////////
//!
//!byte readSerialBuffer(byte *charPtr)
//!{
//!   int errorCode;
//!   
//!   if (gblSerialBufferIsFull == TRUE)
//!   {
//!      gblSerialBufferIsFull = FALSE;
//!      errorCode = SERIAL_OVERFLOW;   
//!      gblSerialBufferPutIndex = 0;
//!      gblSerialBufferGetIndex = 0;
//!      *charPtr = 0;      
//!   }
//!   else if (gblSerialBufferGetIndex == gblSerialBufferPutIndex)   
//!   {
//!       errorCode = SERIAL_NO_DATA;      
//!       *charPtr = 0;
//!   }
//!   else
//!   {
//!      *charPtr = gblSerialBuffer[gblSerialBufferGetIndex];
//!      gblSerialBufferGetIndex++;
//!      if (gblSerialBufferGetIndex >= SERIAL_BUFFER_SIZE)
//!      {  gblSerialBufferGetIndex = 0;
//!      }
//!      errorCode = SERIAL_SUCCESS;   
//!   }
//!   return(errorCode);
//!}
//!
//!
//!int1 serialKbhit() {
//!   return(gblSerialBufferPutIndex != gblSerialBufferGetIndex);
//!}
//!
//!char serialGetChar() {
//!   char foo;
//!   
//!   readSerialBuffer(&foo);
//!   return(foo);
//!   
//!}


/////////////////////////////////////////////////////////////////////////
//
//   A D D - O N   M O D U L E S 
//
/////////////////////////////////////////////////////////////////////////



void DoDisplayModuleStuff(void) {

   // if i2c is being used
   if (gblI2CisBusy)
      return;
      
   // if logo code is being downloaded
   if (gblPauseI2CActivity)
      return;

   // Auto detect a display module
   // gblTimeToProbeDisplayModule increases every 0.1 sec in timer1
   if ((gblTimeToProbeDisplayModule > 10) && (gblAutoDetectDisplays)) {
      // This command locks up the board. It is suspected that the 1k i2c pull-up on the 5.1 board is the cause
      //gblDisplayAddress = displayPing();
      gblDisplayPresent = gblDisplayAddress? 1:0 ;  // gblDisplayPresent=1 if a display is found (gblDisplayAddress !=0)
      gblTimeToProbeDisplayModule = 0;
   }
   
   
///  We no longer send sensor values to i2c display modules. We now use the built-in 7-segment display to show these values   
   
//!   // Send sensor values to the display module
//!   // True if a display module is present or when manual mode is activated from the Logo code
//!   // gblTimeToSendSensorValues is set in Timer2 
//!   if ((gblTimeToSendSensorValues > 3) && (gblDisplayPresent || !gblAutoDetectDisplays)) {
//!      displaySendSensors();
//!      gblTimeToSendSensorValues = 0;
//!   }
}   


/////////////////////////////////////////////////////////////////////////
//
//   M I S C
//
/////////////////////////////////////////////////////////////////////////

void init_variables()
{  
   gblBurstModeBits = 0;

   CMD_STATE = WAITING_FOR_FIRST_HEADER;


   ///////////////////////////////////
   // Logo VM variables
   gblLogoIsRunning=0;         // if set, Lovo VM will start fetching opcode
                     // from EEPROM and run them.
   gblStkPtr=0;      // Stack pointer
   gblInputStkPtr=0; // Procedure input stack pointer
   gblErrFlag=0;     // Error flag. Set mostly by procedures in the Stack.c file


   //////////////////////////////////
   // Firmware and board version
   
   gblDeviceRegister[REG_HARDWARE_ID1] = HARDWARE_ID1;
   gblDeviceRegister[REG_HARDWARE_ID2] = HARDWARE_ID2;
   // gblDeviceRegister[REG_HARDWARE_ID3] = 0; // usuaed at the moment
   
   
   gblDeviceRegister[REG_FIRMWARE_ID1] = FIRMWARE_ID;
   // gblDeviceRegister[REG_FIRMWARE_ID2] = 0;   // unused at the moment
   
   
   


   // init the record pointer to the one last saved in the EEPROM
   // see RECORD in evalopcode.c to see how this value is logged
   #if defined(__PCM__)
   gblRecordPtr = read_ext_eeprom(MEM_PTR_LOG_BASE_ADDRESS) + (read_ext_eeprom(MEM_PTR_LOG_BASE_ADDRESS+1) << 8);
   #elif defined(__PCH__)
   gblRecordPtr = read_program_eeprom(MEM_PTR_LOG_BASE_ADDRESS);
   
   #endif
}


void intro ()
{

    set_pwm1_duty(50);        // make a beep
    output_high(RUN_LED);
    delay_ms(50);
    set_pwm1_duty(0);         // stop the beep
    output_low(RUN_LED);
    delay_ms(150);


    set_pwm1_duty(50);        // make a beep
    USER_LED_ON; 
    delay_ms(50);
    set_pwm1_duty(0);         // stop the beep
    delay_ms(50);
    USER_LED_OFF; 

    set_pwm1_duty(50);        // make a beep
    USER_LED_ON; 
    delay_ms(50);
    set_pwm1_duty(0);         // stop the beep
    delay_ms(50);
    USER_LED_OFF; 


}





void Halt()
{
      stopLogoProcedures();
      beep(); delay_ms(100); beep();
}


void clearMotors() {

   int i,j;

   // Disable both motor chips
   output_low(MOTOR_AB_EN);
   output_low(MOTOR_CD_EN);


   // Init all motors to the coast state
   for (i=0,j=0 ; i<MotorCount ; i++)
   {  // setLow(MotorENPins[i]);       // Mtr Enable pin
      setLow(MotorCWPins[i]);   // Mtr CW pin.
      setLow(MotorCCPins[i]);      // Mtr CCW pin
   }
   
}


void initBoard()
{
   int i,j;

   gblDeviceRegister[REG_ACTIVE_MOTORS] = 0;


   set_tris_a(PIC_TRIS_A);
   set_tris_b(PIC_TRIS_B);
   set_tris_c(PIC_TRIS_C);
   set_tris_d(PIC_TRIS_D);
   set_tris_e(PIC_TRIS_E);
   set_tris_f(PIC_TRIS_F);
   set_tris_g(PIC_TRIS_G);

   // Init the analog ports
   // ** The analog ports still work even after commenting out this line. Very strange.
   //    There is a problem when sAN4 is enabled. The pin A4 (7-seg button) stops working. Pin A4 is sAN6, which doesn't make any sense.
   //setup_adc_ports(sAN0|sAN1|sAN2|sAN3|sAN4|sAN7|sAN10|sAN11);   // use all the 8 ADC ports. Pin names are not continguous on the 18F66J50
   //setup_adc_ports(sAN0|sAN1|sAN2|sAN3|sAN7|sAN10|sAN11);   // use all the 8 ADC ports. Pin names are not continguous on the 18F66J50
   setup_adc( ADC_CLOCK_DIV_64 );
   //setup_adc(ADC_CLOCK_INTERNAL);

   // Init default sensor port
   gblCurSensorChannel=defaultPort;
   set_adc_channel( defaultPort );



   clearMotors();


   // init the serial port
   //ser_init();

      // set RTCC to interrupt every 100ms
      setup_timer_0( T0_INTERNAL | T0_DIV_128 );
      set_rtcc(T0_COUNTER);
      enable_interrupts(INT_TIMER0);

      setup_timer_1 ( T1_INTERNAL | T1_DIV_BY_4 ); // int every (65536-T1_COUNTER) * 4 * 4 * 1/64 uSec
      enable_interrupts(INT_TIMER1);
      set_timer1(0);
                                                    
      // Timer2 Interrups every 16 * 255 * 8 * 4/64 us = 2.55 ms
      // On the old 48MHz PIC, Timer2 Interrups every 16 * 250 * 8 * 4/48 us = 2.66 ms
      setup_timer_2(T2_DIV_BY_16,255,8);
      enable_interrupts(INT_TIMER2);

      #ifdef PIC18F66J50
      // Timer3 peroid = 65536 * 2 * 4/48 us = 10.922 ms
      setup_timer_3( T3_INTERNAL | T3_DIV_BY_2 | T3_CCP2_TO_5); // T3 is used for the IR Capture. 0.6ms = 3600 counts (a SONY remote)
      #endif
      
      #ifdef PIC18F66J94
      // Timer3 peroid = 65536 * 2 * 4/64 us = 8.192 ms
      setup_timer_3( T3_INTERNAL | T3_DIV_BY_2 ); // T3 is used for the IR Capture. 0.6ms = 4800 counts (a SONY remote)
      #endif
      
      
      
      set_timer3(0);
      enable_interrupts(INT_TIMER3);
      
      setup_ccp2( CCP_CAPTURE_FE ); // CCP4 is used for the IR receiver. Init to captuer a falling edge.
      enable_interrupts(INT_CCP2);      

      setup_ccp1(CCP_PWM);   // Configure CCP1 as a PWM (for the beeper)


//      setup_low_volt_detect( LVD_TRIGGER_BELOW | LVD_42 );
//      enable_interrupts(INT_LOWVOLT);

      enable_interrupts(INT_RDA);

      enable_interrupts(GLOBAL);


   /////////////////////////////////////////////////
   //
   //  Logo VM stuff
   //
      init_ext_eeprom();


    intro();

//!   // This delay helps the software on the computer to have enough time to
//!   // recognize that the gogo has been restarted and close the existing 
//!   // COM connection before this new session is started. Without this delay
//!   // the computer program will not have enough time to terminate the existing
//!   // COM session and this new session will be invalid until the user power-cycles
//!   // the gogo 
//!   delay_ms(500);


   // Initialize the USB device but does not stalls. 
   // usb_task() must be called periodically to check
   // when a computer is connected. See usb.h for more info
   usb_init_cs();


}

#if defined(__PCH__)

///////////////////////////////////////////////////////////////////////////////
//
//   F L A S H   M E M O R Y   R O U T I N E S
//
///////////////////////////////////////////////////////////////////////////////

// set pointer to the byte address (not the word address)
void FLASHSetByteAddress(int16 address) {

      // calculate the address of the beginning of the current flash block
      gblFlashBaseAddress = address;
      gblFlashBaseAddress &= ((getenv("FLASH_ERASE_SIZE")-1) ^ 0xFFFF);
      
      // calculate the position within the current flash block
      gblFlashOffsetIndex = address - gblFlashBaseAddress;
      //gblFlashOffsetIndex <<= 1; // ptr points to each byte in the buffer (not word)
      
      gblFlashBufferCounter = 0;  // reset the buffer index
      

}

void FLASHBufferedWrite(int16 InByte) {
      
      gblFlashBuffer[gblFlashBufferCounter++] = (int)InByte;
      //gblFlashBuffer[gblFlashBufferCounter++] = (int)(InByte>>8);  // each readable flash block is 2 bytes (we use only one here).
                    
      // when the buffer contains FLASH_WRITE_SIZE bytes -> write to the actual flash memory
      if (!(gblFlashBufferCounter < getenv("FLASH_WRITE_SIZE") )) {
      
         FLASHFlushBuffer();         
      }
}

// Write directely to the Flash without buffering. This will be slow.

void FLASHWrite(int16 InByte) {
      
      FLASHBufferedWrite(InByte);
      FLASHFlushBuffer();

}


void FLASHFlushBuffer() {

      if (gblFlashBufferCounter > 0) {
         writeFLASH(gblFlashBaseAddress, gblFlashOffsetIndex, gblFlashBufferCounter, gblFlashBuffer);

         FLASHSetByteAddress(gblFlashBaseAddress + gblFlashOffsetIndex + gblFlashBufferCounter );

      }
}


// Writes data to flash memory using block mode
// On PIC16F887, Write Size = 8 Words (16 Bytes), Erase size = 16 Words (32 Bytes)
// On PIC18F458, Write Size = 8 Bytes, Erase size = 64 Bytes
// - Whenever we write to the beginning of an erase block, the entire block (16 words) will be
//   automatically erased (set to 0x3FFF). 
// - So, whenever we want to write some bytes to the flash memory, we need to read the entire
//   erase block into a buffer, modify that buffer, then write it back to the flash memory.



void writeFLASH(int16 memoryBlockAddress, int16 positionInMemoryBlock, int16 len, int *Buffer) {

   int writeBuffer[getenv("FLASH_ERASE_SIZE")];
   int16 i, counter;
   int16 writeLenInThisBlock;
   int1 notDone = 1;

//!   // prevent flash write to the firmware area
//!   if (memoryBlockAddress < RECORD_BASE_ADDRESS) {
//!      while (1) { beep(); delay_ms(1000); }  
//!   }


   do {
   
      // read the entire erase block to memory
      read_program_memory(memoryBlockAddress, writeBuffer, getenv("FLASH_ERASE_SIZE"));
      
      // if write len is longer than the current memory block -> trim it
      if ( len > (getenv("FLASH_ERASE_SIZE") - positionInMemoryBlock)) {
         writeLenInThisBlock = getenv("FLASH_ERASE_SIZE") - positionInMemoryBlock;
      } else {
         writeLenInThisBlock = len;
      }
      
      // modify parts of the block with the new data
      for (i=positionInMemoryBlock, counter=0; counter<writeLenInThisBlock; i++, counter++) {
         writeBuffer[i] = *(Buffer+counter);
      }
      
      // Must disable interrupts during the entired flash write process.
      // Write fails otherwise. I suspect that this is true for the
      // 18F66J50 PIC because it has a large erase block (1024) and it needs
      // to loop through the write process without interruption.
      disable_interrupts(GLOBAL);
      // write the block back to the flash memory. 
      for (i=0 ; i< getenv("FLASH_ERASE_SIZE") / getenv("FLASH_WRITE_SIZE"); i++) {
         
         write_program_memory( memoryBlockAddress + i * getenv("FLASH_WRITE_SIZE") ,
                               writeBuffer + i * getenv("FLASH_WRITE_SIZE"), 
                               getenv("FLASH_WRITE_SIZE"));
      }
      enable_interrupts(GLOBAL);

      
      // if write-data overlaps between memory blocks -> update variables and
      // loop to write the next block
      if ((positionInMemoryBlock + len) > getenv("FLASH_ERASE_SIZE")) {
         memoryBlockAddress += getenv("FLASH_ERASE_SIZE");
         len -= getenv("FLASH_ERASE_SIZE") - positionInMemoryBlock;
         Buffer += getenv("FLASH_ERASE_SIZE") - positionInMemoryBlock;
         positionInMemoryBlock = 0;

      } else {
         notDone = 0;
      }
      output_toggle(USER_LED);
   } while (notDone);
}


#endif



/////////////////////////////////////////////////////////////////////////
//
//   I N P U T    D A T A    H A N D L E R
//
/////////////////////////////////////////////////////////////////////////


void ProcessInput()
{   byte InByte;
   int1 doNotStopRunningProcedure=1;   // if set means we've got a Logo cmd. We need to echo back.
   int i;
   int8 out_data[USB_CONFIG_HID_TX_SIZE];
   int8 in_data[USB_CONFIG_HID_RX_SIZE];   
   int cmd=0;
   int readLength;
   char displayModuleBuffer[33];

   if (usb_enumerated())
   {
 
      if (usb_kbhit(USB_HID_ENDPOINT))
      {
         usb_get_packet(USB_HID_ENDPOINT, in_data, USB_CONFIG_HID_RX_SIZE);
         
         /////////////////////////////////////////////////////////////////
         //
         //  Command Packet
         //
         /////////////////////////////////////////////////////////////////

         if (in_data[0] == CMD_PACKET) {
            cmd = in_data[1];
            switch (cmd) {
               case CMD_MOTOR_ON_OFF:
                  if (in_data[3] == 1) {
                     MotorControl(MTR_ON);
                  } else {
                     MotorControl(MTR_OFF);
                  }
                  break;

               case CMD_MOTOR_DIRECTION:
                  if (in_data[3] == 1) {
                     MotorControl(MTR_THISWAY);
                  } else {
                     MotorControl(MTR_THATWAY);
                  }
                  break;
                  
               case CMD_MOTOR_RD:
                  MotorControl(MTR_RD);
                  break;
               case CMD_SET_POWER:
                  SetMotorPower((in_data[3]<<8) + in_data[4]);
                  break;
               case CMD_SET_SERVO_DUTY:            
                  setServoDuty((in_data[3]<<8) + in_data[4]);
                  break;
                  
               case CMD_SET_ACTIVE_PORTS:
                  TalkToMotor(in_data[2]);
                  break;
               case CMD_TOGGLE_ACTIVE_PORT:
                  ToggleMotorSelection(in_data[2]);
                  break;
               case CMD_BEEP:
                  beep();
                  break;
                  
               case CMD_AUTORUN_STATE:
                  setAutorunState(in_data[2]);
                  break;
                  
               case CMD_LOGO_CONTROL:
                  if (in_data[2] == 0) {
                     stopLogoProcedures();
                  } else if (in_data[2] == 1) {
                     startLogoProcedures();
                  } else if (in_data[2] == 2) {
                     startStopLogoProcedures();
                  } 
                  break;
                  
               case CMD_LED_CONTROL:
                  if ((in_data[3] & 1) == 1) {
                     USER_LED_ON;
                  } else {
                     USER_LED_OFF;
                  }
                  break;
                  
               case CMD_SYNC_RTC:
                  rtcInit();
       
                  for (i=0;i<7;i++) {
                     rtcSetItem(i, in_data[2+i]);
                  }
                  break;
                           
               case CMD_READ_RTC:
                  for (i=0;i<7;i++) {
                     gblDeviceRegister[REG_RTC_SECONDS+i] = rtcGetItem(i);
                  }
                  break;               
                           
               // display module commands
               case CMD_SHOW_SHORT_TEXT:
               case CMD_SHOW_LONG_TEXT:

                  i=0;
                  do {
                     displayModuleBuffer[i] = in_data[2+i];
                  } while(in_data[2+i++] != '\0');

                  if (cmd==CMD_SHOW_SHORT_TEXT) {
                     displayText(DISPLAY_CMD_SEND_TEXT, displayModuleBuffer);
                  } else {
                     displayText(DISPLAY_CMD_SEND_LONG_TEXT, displayModuleBuffer);
                  }
                  break;
                          
               case CMD_CLEAR_SCREEN:
                  clearDisplay();
                  break;
               
               /// voice module commands               
               
               case CMD_VOICE_PLAY_PAUSE:
                  i2cWrite(0xB8, 1, 6); break;
               case CMD_VOICE_NEXT_TRACK:
                  i2cWrite(0xB8, 1, 9); break;
               case CMD_VOICE_PREV_TRACK:
                  i2cWrite(0xB8, 1, 18); break;               
               case CMD_VOICE_GOTO_TRACK:
                  i2cWrite(0xB8, 3, in_data[2]+48); break;      // 48 is ascii for'0'
               case CMD_VOICE_ERASE_ALL_TRACKS:
                  i2cWrite(0xB8, 1, 12); break;               
               
               case CMD_I2C_WRITE:
                  i2cWriteNbytes(in_data[2], in_data[3], in_data[4], &in_data[5]);
                  break;

               case CMD_I2C_READ:
                  // create an I2C report packet
                  gblI2CBuffer[0] = I2C_PACKET;  // Packet type is I2C_PACKET (8)
                  // Fill the Packet with the i2c data
                  if (i2cWriteNbytes(in_data[2], in_data[3], in_data[4], &gblI2CBuffer[2]) == SUCCESS) {
                     gblI2CBuffer[1] = in_data[4];  // Data length
                  } else {
                     gblI2CBuffer[1] = 0;  // length zero indicates an error
                  }
                  
                  
                  gblReadyToSendI2CPacket = 1;  // flag set to send a HID report packet 
                  break;
                        
                         
               case CMD_REBOOT:
                  reset_cpu();
                  break;
            }
   
         }  
      
         /////////////////////////////////////////////////////////////////
         //
         //  FLash Memory Operaion Procedure Packet
         //
         /////////////////////////////////////////////////////////////////
      
         
         else if (in_data[0] == FLASH_MEMORY_OPERATION_PACKET) {

            cmd = in_data[1];
            
            // make sure we stop any running Logo code
            stopLogoProcedures();
            
            switch (cmd) {
               case LOGO_SET_MEMORY_POINTER:
               case FLASH_SET_MEMORY_POINTER:

                  //gblMemPtr = (((int16)in_data[2] << 8) + in_data[3]) << 1;
                  gblMemPtr = (((int16)in_data[2] << 8) + in_data[3]) ;
                  
                  // if setting Logo mem location -> the pos must be relative to
                  // the Logo memory area of the flash memory
                  if (cmd == LOGO_SET_MEMORY_POINTER) {
                     FLASHSetByteAddress((FLASH_USER_PROGRAM_BASE_ADDRESS) + gblMemPtr);
                  } 
                  // if setting Flash mem location -> use the raw memory location.
                  // Caution. Use with care, as this can easily corrupt the firmware
                  // if not used properly.
                  else {
                     FLASHSetByteAddress( gblMemPtr);
                  }
                  //printf(hid_putc,"gblMemPtr = %lu, physical address = %lX\n", gblMemPtr, (FLASH_USER_PROGRAM_BASE_ADDRESS) + gblMemPtr);
                  break;
                  
                  
               
               case MEM_WRITE_BYTES:
                  // in_data[2] = the length of the data to be written
                  for (i=0;i<in_data[2];i++) {
                     // write to the flash buffer 
                     // The data starts at location 3 in the in_data
                     // It automatically writes to the flash
                     // every time the buffer is full
                     FLASHBufferedWrite(in_data[3+i]);
                     // call usb_task() to make sure we don't loose USB connectivity during
                     // the time-consuming flash write operation
                     usb_task();
                  }
                  // write remaining data in the buffer to the flash
                  FLASHFlushBuffer();             
                  //printf(hid_putc,"Written %d bytes\n", in_data[2]);
               
                  break;
               case MEM_READ_BYTES:
                  readLength = in_data[2];
                  sendBytes(gblMemPtr, readLength);
                  break;
            }
      
      }
         /////////////////////////////////////////////////////////////////
         //
         //  Raspberry Pi Commands
         //
         /////////////////////////////////////////////////////////////////
      
         
         else if (in_data[0] == RASPBERRY_PI_CMD_PACKET) {

            cmd = in_data[1];    
            
            switch (cmd) {
               case RPI_SHUTDOWN:
                  rpiAddCmdToBuffer(SHUTDOWN);
                  break;
               case RPI_REBOOT:
                  rpiAddCmdToBuffer(REBOOT);
                  break;
                  
               case RPI_WIFI_CONNECT:
                  rpiCommandStart();
         
                  // Add the command byte
                  rpiAppendOutputBuffer(WIFI_CONNECT);
                  for (i=0;i<in_data[2];i++) {
                     rpiAppendOutputBuffer(in_data[3+i]);
                  }
                  rpiCommandEnd();

                  break;
                  
               case RPI_WIFI_DISCONNECT:
                  rpiAddCmdToBuffer(WIFI_DISCONNECT);
                  break;
                  
               case RPI_CAMERA_CONTROL:
                  if (in_data[2] == 0) {
                     rpiAddCmdToBuffer(CLOSE_CAMERA);
                  } else {
                     rpiAddCmdToBuffer(USE_CAMERA);
                  }
              
                  break;
               case RPI_FIND_FACE_CONTROL:
                  if (in_data[2] == 0) {
                      rpiAddCmdToBuffer(STOP_FIND_FACE);
                  } else {
                      rpiAddCmdToBuffer(START_FIND_FACE);
                  }
               
                  break;
               case RPI_TAKE_SNAPSHOT:
                  if (in_data[2] == 0) {
                     rpiAddCmdToBuffer(TAKE_PREVIEW_IMAGE);
                  } else {
                     rpiAddCmdToBuffer(TAKE_SNAPSHOT);
                  }
               
                  break;
               case RPI_EMAIL_CONFIG:
                  rpiCommandStart();
         
                  // Add the command byte
                  rpiAppendOutputBuffer(EMAIL_CONFIG);
                  for (i=0;i<in_data[2];i++) {
                     rpiAppendOutputBuffer(in_data[3+i]);
                  }
                  rpiCommandEnd();

                  break;
               case RPI_EMAIL_SEND:
                  rpiCommandStart();
         
                  // Add the command byte
                  rpiAppendOutputBuffer(EMAIL_SEND);
                  for (i=0;i<in_data[2];i++) {
                     rpiAppendOutputBuffer(in_data[3+i]);
                  }
                  rpiCommandEnd();

                  break;
               case RPI_SMS_SEND:
                  rpiCommandStart();
         
                  // Add the command byte
                  rpiAppendOutputBuffer(SEND_SMS);
                  for (i=0;i<in_data[2];i++) {
                     rpiAppendOutputBuffer(in_data[3+i]);
                  }
                  rpiCommandEnd();

                  break;  
                  
               case RPI_RFID_INIT:
                  if (in_data[2] == 1) {
                     rpiAddCmdToBuffer(USE_RFID);
                  } else if (in_data[2] == 0) {
                     rpiAddCmdToBuffer(CLOSE_RFID);
                  }

                  break;      
               
               case RPI_RFID_COMMAND:
                  if (in_data[2] == 0) {
                     rpiAddCmdToBuffer(RFID_BEEP);
                  } else if (in_data[2] == 1 ) {
                     rpiCommandStart();
                     // Add the command byte
                     rpiAppendOutputBuffer(RFID_WRITE);
                     rpiAppendOutputBuffer(in_data[3]);
                     rpiCommandEnd();                     
                  }         
                  break;
            }
  
         }
  
      }
   // else assume no data in serial buffer
   }

}



/////////////////////////////////////////////////////////////////////////
//
//   M A I N
//
/////////////////////////////////////////////////////////////////////////


void main() {


int16 SensorVal;
int16 uploadLen,counter;


int16 foo;

int i;

#ifdef PIC18F66J50
   RBPU = 0;   // enable port b internal pull-up.
   PLLEN=TRUE;  // enable the PLL 
   delay_ms(1);
#endif

#ifdef PIC18F66J94
   port_b_pullups(0x01);

#endif



//!while (1) {
//!   output_toggle(USER_LED);
//!   output_toggle(USER_LED2);
//!   delay_ms(1000);
//!}



init_variables();
initBoard();
rPiInit();  // init Raspberry Pi settings

//!while (1) {
//!   
//!
//!
//!   //output_toggle(USER_LED);
//!
//!   if (gblPrintCCPbuffer) {
//!      for (i=0;i<12;i++) {
//!         printf("%lu", gblCCPtest[i]);
//!         delay_ms(2);
//!      }
//!      beep();
//!      gblPrintCCPbuffer=0;
//!      
//!   }
//!   delay_ms(20);
//!
//!}

//  If autorun is set -> run logo procedures
// read_program_eeprom() returns a 16 bit value, eventhough we need only 8
// so, we need to mask and use only the lower 8 bit in order to get the
// correct value.
if ((read_program_eeprom(AUTORUN_STATUS_ADDRESS) & 0xff)==AUTORUN_ON) {
   startStopLogoProcedures();      
} 


while (1) {


   ProcessInput();
    


      DoSensorStuff();
      DoDisplayModuleStuff();
      DoMotorStuff();
      DoDeviceRegisterStuff();
      DoRpiStuff();



  if (gblReadyToSendHIDPacket) {
      
      // send the gogo registers
      usb_put_packet(USB_HID_ENDPOINT, gblDeviceRegister, USB_CONFIG_HID_TX_SIZE, USB_DTS_TOGGLE);
      gblReadyToSendHIDPacket = 0;
                
  }
  
  if (gblReadyToSendI2CPacket) {
      usb_put_packet(USB_HID_ENDPOINT, gblI2CBuffer, USB_CONFIG_HID_TX_SIZE, USB_DTS_TOGGLE);
      gblReadyToSendI2CPacket = 0;  
  }
  
  if (gblReadyToSendRpiHIDPacket) {

      // if we not received any new packets from the Rpi -> we set the
      // hardware revision to 0
      if (gblRpiWatchDog >= RPI_WATCH_DOG_LIMIT) {
         gblRpiRxBufferA[1] = 0; 
         gblRpiRxBufferB[1] = 0;
      }
  
      // Send raspberry pi registers
      if (gblRpiRxUsingBufferA) {
         usb_put_packet(USB_HID_ENDPOINT, gblRpiRxBufferB, USB_CONFIG_HID_TX_SIZE, USB_DTS_TOGGLE);
      } else{ 
         usb_put_packet(USB_HID_ENDPOINT, gblRpiRxBufferA, USB_CONFIG_HID_TX_SIZE, USB_DTS_TOGGLE);
      }      
   
      
      gblReadyToSendRpiHIDPacket = 0;  
      //output_toggle(USER_LED);
  }
  
   
   // if Logo is running
  if (gblLogoIsRunning)
   {
            // if wait command is not in progress -> run next code
            if (!gblWaitCounter && !gblCmdDelayCounter) {
               
               evalOpcode(fetchNextOpcode());
            }
   } else if (flgNeedToTurnOffAllMotors) {
         gblMotorONOFF=0;  // turn off all motors by clearing the on,off flag bits
         //ENLow(0); ENLow(1); // turn off all the motor EN pins   
         clearMotors(); // turn off all motors
         flgNeedToTurnOffAllMotors=0;
   }

//!   ///////////////////////////////////////////////////////////////
//!   // make sure the CMD_STATE is not stuck somewhere for too long
//!
//!   // gblCmdTimeOut is incremented in Timer1's ISR every 0.1 sec
//!   if (gblCmdTimeOut > CMD_TIMEOUT_PERIOD) {
//!      CMD_STATE = WAITING_FOR_FIRST_HEADER;
//!      gblCmdTimeOut = 0;
//!      
//!   }

   ///////////////////////////////////////////////////////////////
   // Do USB Stuff
   // Most importantly -> check for USB connection with a computer

   usb_task();




}

}


#include <gogo_hid.c>
#include <logovm.c>
#include <i2cDisplay.c>
//#include <bootload.c>
#include <i2c.c>
#include <ds1307.c>
#include <7segment.c>
#include <raspberrypi.c>
