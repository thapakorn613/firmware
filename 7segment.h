#define  EN_DIGIT1   PIN_F7
#define  EN_DIGIT2   PIN_E3
#define  EN_DIGIT3   PIN_E4
#define  EN_DIGIT4   PIN_E0

#define  SEG_A       PIN_E1
#define  SEG_B       PIN_E2
#define  SEG_C       PIN_G3
#define  SEG_D       PIN_G1
#define  SEG_E       PIN_G0
#define  SEG_F       PIN_E5
#define  SEG_G       PIN_G4
#define  SEG_DP      PIN_G2
#define  PUSH_BUTTON_PIN_7SEG    PIN_A4

#define BANNER_DISPLAY_DURATION     10

void showNextDigit();
void turnOffAllSegments();
int Char2Segments(char inChar);
void cls_internal7Seg();
void generate7SegmentDisplayBufferFromStirng(char *string);
void generate7SegDisplayBuffer(int16 val);
void showSensorOn7SegDisplay(int16 val);
void showNextDigit();
void check7SegmentButton();
void update7SegementBuffer();

int gblDisplayDigit=0;
int gbl7SegButtonAlreadyPressed=0;  
int gblBannerCounter = 0;  // tracks time to show page banners
int gblDisplayPage = 0; // the current page to show
int gblPrevDisplaypage = 0; // track page change
int16 gblPrev7SegSensorVal=0;  // track sensor value changes
int1 gblForce7SegUpdate=1; // when true will hide the banner of that page

int gbl7SegPage0Buffer1=0;
int gbl7SegPage0Buffer2=0;
int gbl7SegPage0Buffer3=0;
int gbl7SegPage0Buffer4=0;

int1 gblNeedToShowGoGoBanner = 1; // flag to tell the board to show the 
                                  // banner text 'gogo' upon power up


int gbl7SegBuffer1 = 0;  
int gbl7SegBuffer2 = 0;  
int gbl7SegBuffer3 = 0;  
int gbl7SegBuffer4 = 0;  
