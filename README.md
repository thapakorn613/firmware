# Firmware

Firmware for the GoGo Board 5 series. 

Building the Microchip PIC18F source requires CCS PIC-C compiler. The main file is rpt10.c


GoGo Board 5 (formally Pi-Topping) firmware change log
=======================================================

All the changes are accumulative unless noted. 

Version 42 - Mar, 2018
- Logo code is now cached in RAM for execution, which improves I/O stability.
  For example, servo motors are significantly more stable. Previsously, Logo code is stored in 
  the PIC's flash memory. Hence, during execution, the next command must always be retrieved from flash.
  This is a time consuming and un-interruptable I/O operation. Moving the Logo code to RAM eliminates
  this I/O lag. Upon starting Logo execution, the Logo binary code is read from flash and stored into a 
  buffer in RAM.
  * The down side is that the available RAM is smaller than that reserved in flash memory. Thus, now we
    only have 3/4 the space we had for storing Logo code (768 bytes, down from 1024 bytes).

- Added I2C Read and Write commands in tethered mode. I2C devices can now be controlled in tethered mode.
  A new packet type (8) has been added which will contain data from an I2C read command. Here's the packet
  format:
    Byte 0 = 8
    Byte 1 = length
    Byte 2 ...  = data 


Version 41 - July, 2017
- Added get-key-int-value command which is essentially a atol() of the string key value.
- Added I2C-start command to support low level i2c communications. i2c-stop/write/read is already there.


Version 40 - May, 2016
- Added: A send_message command for the GoGo to send topic/message pairs to the Raspberry Pi
  sendmessage "topic1" 1 1234
  sendmessage "topic2" 2 "test"

  the first parameter is the topic name string.
  The second parameter determins if the message is a number or string 1=number, 2=string
  The thrid parameter is the message.

- Changed: The random command now takes a 'max number' parameter
      random 10
    will return a psudo-random number from 0 to 9.
- Changed: Read sensor and switch opcodes have been optimized. Instead of having one opcode
  for each port (sensor1, sensor2, ... ), we now implement a single 'read_sensor(x)' command
  where 'x' is the desired sensor number. The same goes for switch as well.
  ** This means that this firmware will be compatible with the new gogo widget only (v1.4.0 or newer)


Version 32 - Dec 15, 2015
- Added additional safety measures to i2c read/write commands. Hopefully this can prevent
  lockups when using i2c extension modules.

Version 31 - Nov 16, 2015
- Fixed a minor bug that prevents the built-in 7-segment display to work when there is 
  no external display attached.


Version 30 - Nov 15, 2015
- Now the "show" command displays on both the built-in 7-segment and the i2c 16x2 display module.

Version 29 - Sep 21, 2015
- Added Tick timers. These are used to allow intervals in a logo program.
-- "settickrate 10"   will tick every 1 second
-- "tickcount"  will report how many ticks have passed
-- "cleartick"  will reset the ticks

- Changed: all numbers sent to the Raspberry Pi are now sent as a string
-- Previously they were sent as binary data, which causes errors with 
   values that collide with special characters, such as new line (13).





Version 28 - July 13, 2015
- Fixed: send mail command now takes three string parameters.
  sendmail address, title, body


Version 27 - July 13, 2015
- Fixed a problem with the "show" command where text is displayed
  in reverse.
- Changed the boot beep pattern to three beeps instead of two.
- Changed. Now the screen displays 'GoGo' upon boot.


Version 26 - May 28, 2015
- Fixed an IR reciever bug.


Version 25 - April 28, 2015
- Added: key-value comparison. Logo example

  if key "button" = "forward" [ab, on]

  keys index and values are sent to the board from the Raspberry Pi

- Added: key-int value

  set count intkey "slider"

- Added: clearkeys, which will clear all key values.
  


Version 24 - Mar 23, 2015
- Added: Logo 'SAY' text-to-speech primitive
  say "hello"
- Added: Logo "USESMS" and "SENDSMS" text message primitives
  usesms
  sendsms "0811234567" "Hi there"
- Added: Logo "modulo" operator
  5 % 3  # result is 2
- Added: new reporters: on?, thisway? or cw?, thatway? or ccw?, power
  aon?  - true if port a is on
  abon? - true if port a AND b are both on
  abcdon? - true if all ports are on
  athisway? - true if port a's direction is set to 'thisway'
  abthatway? - true if port a and b's directions are both 'thatway'
  apower - returns the current power level of port a. port name can be a-d. Can't use multiple port names
           I.e. 'abpower' is invalid

- Added: a "if-state-change" statement. The if executes only if the condition's state changes from false to true
   if sensor1 > 500 [beep]  # always beeps when sensor1 is greater than 500
   ifstatechange sensor1 > 500 [ beep] # beep only when sensor1 becomes greater than 500 for the first time

- Added: <= (less or equal) and >= (greater or equal) operators


Version 21 -
- 'show' now works with external display modules (the 16x2 LCD module).

Version 20 -
- i2c read problems fixed
- Added Autorun support
- Added Logo Run/Stop control from the widget
- USB Rx buffer has been expanded to 64 bytes (was 32)

Raspberry Pi functionalities:
- Added RFID (model SL 500) commands
- Added SMS commands
- Added send e-mail commands (using Gmail)


Version 9 -
- IR receiver now works

Version 8 - Aug 14, 2014
- Firmware self-update now works.

Version 7 - July 26, 2014
- Fixed a bug with the 7 segment display


Version 6 - July 25, 2014
- Integrated the HID firmware
- The built-in 7 segment display now has all its functions.


Version 2 - Feb 14, 2014
- Invalid Logo code won't halt the board anymore. Same patch as in GoGo firmware v.18


Version 1 - June 9, 2013
- First working version.
