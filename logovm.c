//
// logovm.c - Logo compiler support routines
//
// Copyright (C) 2001-2007 Massachusetts Institute of Technology
// Contact   Arnan (Roger) Sipiatkiat [arnans@gmail.com]
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//


/////////////////////////////////////////////////////
//
//  Logo Virtual Machine
//
//  Written by Arnan (Roger) Sipitakiat
//
//  Logo running on the GoGo Board was created based
//  on the Cricket Logo.
//
//  Cricket Logo creators include:
//    Fred G. Martin
//    Brian Silverman
//    Mitchel Resnick
//    Robbie Berg
//
/////////////////////////////////////////////////////


#include <evalOpcode.c>      // This is the Opcode evaluator
#include <stack.c>         // Push/Pop functions for Op-code execution




void sendBytes(unsigned int16 memPtr, unsigned int16 count)
{
   while (count-- > 0) {

   #if defined(__PCM__)
      printf(active_comm_putc, "%c",read_program_eeprom(FLASH_USER_PROGRAM_BASE_ADDRESS + memPtr++));
   #elif defined(__PCH__)
      printf(active_comm_putc, "%c",read_program_eeprom( FLASH_USER_PROGRAM_BASE_ADDRESS + (memPtr++<<1)));
   #endif   

   }
}


unsigned char fetchNextOpcodefromFlash(int16 index)
{
   
   unsigned int16 opcode;
   

   
   #if defined(__PCM__)
      opcode = read_program_eeprom(FLASH_USER_PROGRAM_BASE_ADDRESS + index);
   #elif defined(__PCH__)
  
      // read_program_eeprom() always returns two bytes. This is defined by PIC-C, although
      // it can read from both an odd and even memory address. Doesn't make much sense but
      // it is how it is. In practice, when only one byte is required, simply use the lower byte.                        
      opcode = read_program_eeprom(FLASH_USER_PROGRAM_BASE_ADDRESS + index);
      opcode = opcode & 0xff;
      //printf(hid_putc,"op addr = %Lx, op = %Lx\r\n", FLASH_USER_PROGRAM_BASE_ADDRESS + (gblMemPtr-1), opcode);
   #endif   
   return (int8)opcode;


   
}


unsigned char fetchNextOpcode() {

   // if an ONFOR command was launched we must turn motor off before
   // continuing. When gblONFORNeedsToFinish is falged, we simulate
   // a motor off command.
   if (gblONFORNeedsToFinish) {
      gblONFORNeedsToFinish = 0;
      return(M_OFF);
   } else {
      return (gblByteCodeBuffer[gblMemPtr++]);
   }

}
